function getRadians(degrees) {
    return (Math.PI / 180) * degrees;
}

const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');

let canDrawLine = false;
let lastPointX = 0;
let lastPointY = 0;

let typeOfPainting = 1;

document.body.addEventListener("click", paintLine);

function paintLine(event) {
    coordinateX = event.clientX;
    coordinateY = event.clientY;

    if (canDrawLine){
        if(typeOfPainting == 1){
            ctx.beginPath();
            ctx.moveTo(lastPointX, lastPointY);
            ctx.lineTo(coordinateX, coordinateY);
            ctx.stroke();
            canDrawLine = false;
        }
        else{
            ctx.beginPath();
            ctx.moveTo(lastPointX, lastPointY);
            ctx.lineTo(coordinateX, coordinateY);
            ctx.stroke();

            lastPointX = coordinateX;
            lastPointY = coordinateY;
        }
    }
    else{
            lastPointX = coordinateX;
            lastPointY = coordinateY;
            canDrawLine = true;
    }
}