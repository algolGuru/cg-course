function getRadians(degrees) {
    return (Math.PI / 180) * degrees;
}

const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');


ctx.rect(10, 10, 100, 100);
ctx.fill();