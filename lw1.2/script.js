function getRadians(degrees) {
    return (Math.PI / 180) * degrees;
}

const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');

ctx.beginPath();
ctx.rect(10, 10, 800, 2);
ctx.fill();

ctx.beginPath();
ctx.rotate(45 * Math.PI / 180);
ctx.rect(165, -150, 300, 2);
ctx.fill();
ctx.setTransform(1, 0, 0, 1, 0, 0);

ctx.beginPath();
ctx.rotate(45 * Math.PI / 180);
ctx.rect(150, -135, 300, 2);
ctx.fill();
ctx.setTransform(1, 0, 0, 1, 0, 0);


ctx.beginPath();
ctx.rect(100, 200, 700, 200);
ctx.fillStyle = "#5c3301"
ctx.fill();

ctx.beginPath();
ctx.rect(150, 250, 50, 50);
ctx.fillStyle = "#fff";
ctx.rect(250, 250, 50, 50);
ctx.fillStyle = "#fff";
ctx.rect(350, 250, 50, 50);
ctx.fillStyle = "#fff";
ctx.rect(450, 250, 50, 50);
ctx.fillStyle = "#fff";
ctx.rect(550, 250, 50, 50);
ctx.fillStyle = "#fff";
ctx.rect(650, 250, 50, 50);
ctx.fillStyle = "#fff";
ctx.fill();

ctx.beginPath();
ctx.arc(250, 400, 35, 0, getRadians(360));
ctx.fillStyle = "#fff";
ctx.lineWidth = 5;
ctx.fill();
ctx.stroke();

ctx.beginPath();
ctx.arc(650, 400, 35, 0, getRadians(360));
ctx.fillStyle = "#fff";
ctx.lineWidth = 5;
ctx.fill();
ctx.stroke();

function getRadians(degrees) {
	return (Math.PI / 180) * degrees;
}