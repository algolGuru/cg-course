import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstTaskComponent } from './first-task/first-task.component';
import { HttpClientModule } from '@angular/common/http';
import { SecondTaskComponent } from './second-task/second-task.component';


@NgModule({
  declarations: [
    AppComponent,
    FirstTaskComponent,
    SecondTaskComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
