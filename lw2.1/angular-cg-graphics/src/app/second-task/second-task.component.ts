
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-task',
  templateUrl: './second-task.component.html',
  styleUrls: ['./second-task.component.css']
})
export class SecondTaskComponent implements OnInit{
  @ViewChild('canvas', { static: true })
  canvas!: ElementRef<HTMLCanvasElement>;  

  private ctx!: CanvasRenderingContext2D | null;

  private url: any;

  ngOnInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
  }
	
	selectFile(event: any) {
		var reader = new FileReader();
		reader.readAsDataURL(event.target.files[0]);


		reader.onload = (_event) => {
      var image = new Image();
      image.src = `${reader.result}`;
      image.onload = () => {

        image.width = 1000;
        image.height = 600;
        this.ctx?.drawImage(image, 0, 0);
      }
		}

    var image = new Image();
    image.src = this.url;

	}

  saveFile()
  {
    console.log(this.canvas)
    if(this.canvas != null){
      var image = this.canvas.nativeElement.toDataURL("image/png").replace("image/png", "image/octet-stream");
      window.location.href=image;
    }
  }
}
